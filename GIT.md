```
Command line instructions
You can also upload existing files from your computer using the instructions below.

Git global setup
git config --global user.name "Henrik Bach"
git config --global user.email "bach.henrik@gmail.com"

Use an existing repository
git clone git@gitlab.com:HenrikBach1/testing-python.git
cd testing-python

Create a new repository
git clone git@gitlab.com:HenrikBach1/testing-python.git
cd testing-python
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:HenrikBach1/testing-python.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:HenrikBach1/testing-python.git
git push -u origin --all
git push -u origin --tags
```