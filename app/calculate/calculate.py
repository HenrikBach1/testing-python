import sys


class Calculate(object):
    def add(self, x, y):
        try:
            # Testing pre-conditions
            assert (type(x) == int and type(y) == int)

            return x + y

            # Testing post-conditions
        except AssertionError:
            raise TypeError("Invalid types for arguments: x: {} and y:{}. Only intergers are supported".format(type(x), type(y)))
        except:
            print ("Exception: {}".format(sys.exc_info()[0]))
            raise