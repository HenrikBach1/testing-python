# python app/cli/app.py

import context
import app.calculate.calculate as calculate # Simulate import a module


# main():
if __name__ == '__main__': #pragma: no cover
    calc = calculate.Calculate()
    result = calc.add(2, 2)
    print(result)