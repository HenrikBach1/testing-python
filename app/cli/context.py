import sys
import os


# https://stackoverflow.com/questions/14132789/relative-imports-for-the-billionth-time: Script vs. Module
gettrace = getattr(sys, 'gettrace', None)
if gettrace():
    # Debugging (F5) in VSC...
    # Use __package__ visibility
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__package__), '.')))
elif gettrace is not None:
    # Running (Ctrl+F5) in VSC.. 
    # Running in terminal: 
    #   pipenv shell
    #   pipenv install 
    #   python app/cli/__main__.py
    # Use __file__ visibility
    # VSC: from ...app -> ../..
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))
    # print (sys.path[0])
