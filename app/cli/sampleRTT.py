#sampleRTT.py
import random

alpha = 1/8         #  [RFC 6298]
beta = 1/4
estimatedRTT = 0
devRTT = 0
G = 1
K = 4

sampleRTT = random.randrange(1,100,1)
for i in range(1,100):
    # RFC 6298
    if (i == 1):
        # First time computation of timeoutInterval based on sampleRTT
        estimatedRTT = sampleRTT
        devRTT = sampleRTT/2
        timeoutInterval = estimatedRTT + max (G, K*devRTT)
    else:
        # Subsequent computations of timeoutInterval based on sampleRTT
        devRTT = (1-beta)*devRTT + beta*abs(sampleRTT-estimatedRTT)
        estimatedRTT = (1-alpha)*estimatedRTT + alpha*sampleRTT
        timeoutInterval = estimatedRTT + K*devRTT

    # Printout computed values based on sampleRTT
    # TODO: https://opensource.com/article/20/2/python-gnu-octave-data-science :
    print("step {}: sampleRTT={} estimatedRTT={} timeoutInterval={} {}".format(i, sampleRTT, estimatedRTT, timeoutInterval, timeoutInterval >= sampleRTT))

    # Simulate next sampleRTT value
    if (25 <= i and i <= 49):
        # sampleRTT = 10
        sampleRTT = random.randrange(1,100,1)
    else:
        # sampleRTT = 100
        sampleRTT = random.randrange(1,100,1)

