def sum(m,n) -> int:
    if n <= 0:
        return m
    else:
        return sum(m,n-1)+(m+n)
