import unittest

# import context # Include this module, if debugging this file from inside VS with F5 other than VS Testing Facilities
import app.tests.unit.app.calculate.context
import app.calculate.calculate as calculate


class TestCalculate(unittest.TestCase):
    def setUp(self):
        self.calc = calculate.Calculate()

    def test_add_method_return_correct_result_with_integers(self):
        self.assertEqual(4, self.calc.add(2, 2))

    def test_add_method_raise_exception_with_strings(self):
        self.assertRaises(TypeError, self.calc.add, "Hello", "world")

if __name__ == '__main__':
    unittest.main()
